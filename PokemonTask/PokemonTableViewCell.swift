//
//  PokemonTableViewCell.swift
//  PokemonTask
//
//  Created by Eldar Haseljic on 4/29/20.
//  Copyright © 2020 Eldar Haseljic. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(from pokemon: Pokemon) {
        nameLabel.text = pokemon.name
        numberLabel.text = pokemon.id
        imgView.load(from: pokemon.imageURL!)
    }
}
