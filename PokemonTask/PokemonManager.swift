//
//  PokemonManager.swift
//  PokemonTask
//
//  Created by Eldar Haseljic on 4/30/20.
//  Copyright © 2020 Mirza Alagic. All rights reserved.
//

import Foundation
import UIKit

enum Pokemon_error:Error{
    case noDataAvailable
    case canNotProcessData
}

class PokemonManager {
    static let shared = PokemonManager()
     
    let resourseUrl:URL
    
    //Initializer access level change now
    
    private init() {
           let resourse_string = "http://www.mocky.io/v2/5eaaba2b2d00005f2b268902"
           guard let resourseUrl = URL(string: resourse_string) else {fatalError()}
           self.resourseUrl = resourseUrl
       }
    
    func getPokemonNames(completion: @escaping(Result<[Pokemon], Pokemon_error>) -> Void){
        let dataTask = URLSession.shared.dataTask(with: resourseUrl) {data, _ , _ in
            guard let jsonData = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do{
                let pokemonsResponse = try JSONDecoder().decode([Pokemon].self, from: jsonData)
                var i = 0;
                var sorted = pokemonsResponse.sorted {$0.id.localizedStandardCompare($1.id) == .orderedAscending}

               // print(sorted)
                while(i < sorted.count)
                {
                    sorted[i].imageURL = URL(string: String(format: "https://img.pokemondb.net/artwork/%@.jpg", sorted[i].name.lowercased().addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!))
                    sorted[i].id = "#" + sorted[i].id
                    i += 1
                }
                //print(pokemonsResponse)
                completion(.success(sorted))
            }catch{
                completion(.failure(.canNotProcessData))
            }
        }
        dataTask.resume()
    }
}
