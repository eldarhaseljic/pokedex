//
//  SecondViewController.swift
//  PokemonTask
//
//  Created by Eldar Haseljic on 4/29/20.
//  Copyright © 2020 Eldar Haseljic. All rights reserved.
//


import UIKit

class SecondViewController: UIViewController {

    @IBAction func clicked(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBOutlet weak var Pokemon_collectionView: UICollectionView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
   private var pokemons : [Pokemon] = []
       private var filteredPokemon  = [Pokemon](){
                 didSet{
                     DispatchQueue.main.async {
                          self.Pokemon_collectionView.reloadData()
                            self.loading.stopAnimating()
                            self.loading.isHidden = true
                    }
                 }
             }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Pokemon_collectionView.dataSource = self;
        Pokemon_collectionView.delegate = self;
        navigationItem.hidesBackButton = true;
        //loadJSON()
        
        PokemonManager.shared.getPokemonNames(){
            allPokemons in
            switch allPokemons {
            case .failure(let error):
                print(error)
            case .success(let pok):
                self.pokemons = pok
                self.filteredPokemon = pok
            }
        }
        self.loading.startAnimating()
        self.loading.isHidden = false
        
        let layout = self.Pokemon_collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0,left: 10,bottom: 0,right: 10)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (self.Pokemon_collectionView.frame.size.width - 20)/2, height: self.Pokemon_collectionView.frame.size.height/3)
        }
    
//          
//        private func loadJSON() {
//            let url = Bundle.main.url(forResource: "pokemon_names", withExtension: "json")
//                    
//            guard let jsonData = url else{return}
//            guard let data = try? Data(contentsOf: jsonData) else { return }
//            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else { return }
//            parseJSON(from: json as! [[String: Any]])
//        }
//        
//        private func parseJSON(from dictionary: [[String: Any]]) {
//            for pokemonItem in dictionary {
//                let pokemon = Pokemon(id: pokemonItem["id"] as! String, name: pokemonItem["name"] as! String)
//                pokemons.append(pokemon)
//            }
//            filteredPokemon = pokemons
//            Pokemon_collectionView.reloadData()
//        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SecondViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredPokemon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "second_cell", for: indexPath) as! CollectionViewCell
        cell.configure(from: filteredPokemon[indexPath.row])
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        return cell
    }
}

extension SecondViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filteredPokemon = pokemons
        } else {
            let lowercasedSearch = searchText.lowercased()
            filteredPokemon = pokemons.filter({ pokemon -> Bool in
                return pokemon.name.lowercased().contains(lowercasedSearch)
            })
        }
    }
}
