//
//  ViewController.swift
//  PokemonTask
//
//  Created by Eldar Haseljic on 4/29/20.
//  Copyright © 2020 Eldar Haseljic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private var pokemons : [Pokemon] = []
    private var filteredPokemon  = [Pokemon](){
              didSet{
                  DispatchQueue.main.async {
                      self.pokedexTableView.reloadData()
                    self.loading.stopAnimating()
                    self.loading.isHidden = true
                  }
              }
          }
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet var pokedexTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true;
     //   loadJSON()
      
        PokemonManager.shared.getPokemonNames(){
            allPokemons in
           
            switch allPokemons {
            case .failure(let error):
                print(error)
            case .success(let pok):
                self.pokemons = pok
                self.filteredPokemon = pok
            }
        }
        loading.startAnimating()
        loading.isHidden = false
    }
//
//    private func loadJSON() {
//        let url = Bundle.main.url(forResource: "pokemon_names", withExtension: "json")
//
//        guard let jsonData = url else{return}
//        guard let data = try? Data(contentsOf: jsonData) else { return }
//        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else { return }
//        parseJSON(from: json as! [[String: Any]])
//    }
//
//    private func parseJSON(from dictionary: [[String: Any]]) {
//        for pokemonItem in dictionary {
//            let pokemon = Pokemon(name: pokemonItem["name"] as! String, id: pokemonItem["id"] as! String)
//            pokemons.append(pokemon)
//        }
//        filteredPokemon = pokemons
//        pokedexTableView.reloadData()
//    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PokemonTableViewCell", for: indexPath) as! PokemonTableViewCell
        cell.configure(from: filteredPokemon[indexPath.row])
        return cell
    }
    
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       if searchText.isEmpty {
                   filteredPokemon = pokemons
               } else {
            let lowercasedSearch = searchText.lowercased()
            filteredPokemon = pokemons.filter({ pokemon -> Bool in
                return pokemon.name.lowercased().contains(lowercasedSearch)
            })
        }
    }
}
