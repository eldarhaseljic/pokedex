//
//  Pokemon.swift
//  PokemonTask
//
//  Created by Eldar Haseljic on 4/29/20.
//  Copyright © 2020 Eldar Haseljic. All rights reserved.
//


import Foundation

struct Pokemon:Decodable {
    var id: String
    var name: String
    var imageURL: URL?
    
//    init(name: String, id: String) {
//        self.name = name
//        self.id = String(format: "#%@", id)
//        imageURL = URL(string: String(format: "https://img.pokemondb.net/artwork/%@.jpg", name.lowercased().addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!))
//    }
}
