//
//  CollectionViewCell.swift
//  PokemonTask
//
//  Created by Eldar Haseljic on 4/29/20.
//  Copyright © 2020 Mirza Alagic. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var Pokemon_image: UIImageView!
    @IBOutlet weak var Pokemon_title: UILabel!
    @IBOutlet weak var Pokemon_id: UILabel!
    
    func configure(from pokemon: Pokemon) {
        Pokemon_title.text = pokemon.name
        Pokemon_id.text = pokemon.id
        Pokemon_image.load(from: pokemon.imageURL!)
    }
}
