//
//  UIImageView+Extension.swift
//  PokemonTask
//
//  Created by Eldar Haseljic on 4/29/20.
//  Copyright © 2020 Eldar Haseljic. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func load(from url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
